package com.rishat.shamsutdinov.teachup.data.repository

import android.content.ContentValues.TAG
import android.util.Log
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.rishat.shamsutdinov.teachup.data.remote.FirebaseService
import com.rishat.shamsutdinov.teachup.data.remote.SheetsService
import com.rishat.shamsutdinov.teachup.domain.entity.*
import com.rishat.shamsutdinov.teachup.domain.mappers.SheetApiResponceMapper
import com.rishat.shamsutdinov.teachup.domain.repository.FireBaseRepository
import kotlinx.coroutines.tasks.await
import timber.log.Timber
import javax.inject.Inject

class FireBaseRepositoryImpl @Inject constructor(
    private val mAuth: FirebaseAuth,
    private val databaseApi: FirebaseService,
    private val sheetApi: SheetsService
) :
    FireBaseRepository {
    override suspend fun logIn(email: String, password: String): AuthResult =
        mAuth.signInWithEmailAndPassword(email, password).await()

    override suspend fun signUp(email: String, password: String): String {
        return mAuth.createUserWithEmailAndPassword(email, password).await().user!!.uid
    }

    override suspend fun createNewTeacher(teacher: Teacher) {
        databaseApi.createTeacher(teacher.uid, teacher)
    }

    override suspend fun createNewStudent(student: Student) {
        databaseApi.createStudent(student.uid, student)
    }

    override suspend fun getStudentTasks(sheetId: String): ArrayList<StudentTask> {
        return SheetApiResponceMapper.map(sheetApi.getTable(sheetId).await().body()!!.values)
    }

    override suspend fun createGroup(code: String, studyGroup: StudyGroup) {
        databaseApi.createNewGroup(code, studyGroup)
    }

    override suspend fun createChat() {
        TODO("Not yet implemented")
    }
}