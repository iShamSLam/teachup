package com.rishat.shamsutdinov.teachup

import android.app.Application
import com.rishat.shamsutdinov.teachup.di.component.AppComponent
import com.rishat.shamsutdinov.teachup.di.component.DaggerAppComponent
import com.rishat.shamsutdinov.teachup.di.module.AppModule
import timber.log.Timber

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()

        if (BuildConfig.DEBUG) {
            Timber.plant(object : Timber.DebugTree() {
                override fun createStackElementTag(element: StackTraceElement): String? {
                    return "(${element.fileName}:${element.lineNumber}).${element.methodName}"
                }
            })
        }
    }
}