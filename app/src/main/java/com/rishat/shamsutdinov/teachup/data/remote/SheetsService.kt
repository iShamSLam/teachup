package com.rishat.shamsutdinov.teachup.data.remote

import com.rishat.shamsutdinov.teachup.domain.entity.SheeApiResponce
import com.rishat.shamsutdinov.teachup.domain.entity.Student
import com.rishat.shamsutdinov.teachup.domain.entity.StudentTask
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface SheetsService {

    @GET("https://sheets.googleapis.com/v4/spreadsheets/{spreadsheetId}/values/A1:Z1000?key=AIzaSyBuSxwGgnb834vQKp4J1f-NfLfEAZb7RBA")
    fun getTable(@Path("spreadsheetId")id : String) : Deferred<Response<SheeApiResponce>>
}