package com.rishat.shamsutdinov.teachup.di.module

import android.app.Activity
import android.content.Context
import com.rishat.shamsutdinov.teachup.di.qualifier.ActivityContext
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    fun provideActivity(): Activity = activity

    @Provides
    @ActivityContext
    fun provideContext(): Context = activity
}
