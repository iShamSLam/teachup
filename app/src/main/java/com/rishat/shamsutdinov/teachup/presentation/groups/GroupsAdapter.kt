package com.rishat.shamsutdinov.teachup.presentation.groups

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rishat.shamsutdinov.teachup.R
import com.rishat.shamsutdinov.teachup.domain.entity.StudyGroup
import com.rishat.shamsutdinov.teachup.utils.inflate
import kotlinx.android.synthetic.main.item_group.view.*

class GroupsAdapter : RecyclerView.Adapter<GroupsAdapter.GroupViewHolder>() {

    interface ClickListener {
        fun onGroupTouched(code : String, name : String)
    }

    var listener: ClickListener? = null

    var data: ArrayList<StudyGroup> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class GroupViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(studyGroup : StudyGroup) = itemView.apply {
            group_name.text = studyGroup.name
            student_count.text = studyGroup.student.size.toString()

            this.setOnClickListener{
                listener?.onGroupTouched(studyGroup.code_phrase, studyGroup.name)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupViewHolder {
        return GroupViewHolder(parent.inflate(R.layout.item_group))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: GroupViewHolder, position: Int) {
        holder.bind(data[position])
    }
}