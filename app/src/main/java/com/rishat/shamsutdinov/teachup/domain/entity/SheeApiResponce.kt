package com.rishat.shamsutdinov.teachup.domain.entity

import com.google.gson.annotations.SerializedName

data class SheeApiResponce(
    @SerializedName("range") val range : String,
    @SerializedName("majorDimension") val majorDimension : String,
    @SerializedName("values") val values : List<List<String>>
)