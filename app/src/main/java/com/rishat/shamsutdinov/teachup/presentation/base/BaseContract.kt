package com.rishat.shamsutdinov.teachup.presentation.base

import android.app.Activity
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.navigation.NavDirections
import com.rishat.shamsutdinov.teachup.utils.PermissionUtil

interface BaseContract {

    interface View {
        fun activity(): Activity
        fun showMessage(@StringRes stringResId: Int)
        fun navigate(@IdRes resId: Int)
        fun navigate(navDirections: NavDirections)
        fun popBackStack(): Boolean
        fun getStringRes(@StringRes stringResId: Int): String
        fun hideKeyboard()
        fun isPermissionGranted() : Boolean
        fun requestRequiredPermissions()
        fun getPermissionsState(): PermissionUtil.PermissionState
        fun showRequestPermissionsSnackbar(@StringRes stringResId: Int)
    }

    interface Presenter<in V : View> {
        fun attachView(view: V)
        fun detachView()
        fun isViewAttached(): Boolean
    }

}