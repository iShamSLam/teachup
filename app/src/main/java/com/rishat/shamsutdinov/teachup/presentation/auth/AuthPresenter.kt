package com.rishat.shamsutdinov.teachup.presentation.auth

import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.rishat.shamsutdinov.teachup.R
import com.rishat.shamsutdinov.teachup.data.remote.SheetsService
import com.rishat.shamsutdinov.teachup.data.repository.FireBaseRepositoryImpl
import com.rishat.shamsutdinov.teachup.di.scope.FragmentScope
import com.rishat.shamsutdinov.teachup.domain.entity.Student
import com.rishat.shamsutdinov.teachup.domain.entity.Teacher
import com.rishat.shamsutdinov.teachup.presentation.base.BasePresenter
import com.rishat.shamsutdinov.teachup.utils.SharedPreferencesUtil
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await
import java.lang.Exception
import javax.inject.Inject

@FragmentScope
class AuthPresenter @Inject constructor(
    private val sharedPreferencesUtil: SharedPreferencesUtil,
    private val repositoryImpl: FireBaseRepositoryImpl,
    private val data: SheetsService
) :
    BasePresenter<AuthContract.View>(), AuthContract.Presenter {

    companion object {
        private const val EMAIL_KEY: String = "email_key"
        private const val PASSWORD_KEY: String = "password_key"
    }

    private var uid: String = ""
    private var teacher: Teacher = Teacher()
    private var student: Student = Student()

    override fun logInWithEmailAndPassword(email: String, password: String) {
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val m = repositoryImpl.logIn(email, password)
                if (m.user != null) {
                    saveToPreferences(email, password)
                    launch(Dispatchers.Main) {
                        view?.showMessage(R.string.login_succes)
                        view?.hideLoginAction()
                    }
                    delay(1000)
                    view?.navigate(AuthFragmentDirections.actionAuthFragmentToGroupsFragment(uid))
                }
            } catch (e: Exception) {
                launch(Dispatchers.Main) { view?.showMessage(R.string.login_error) }
            }
        }
    }

    override fun signUpNewUser(email: String, password: String) {
        GlobalScope.launch {
            uid = repositoryImpl.signUp(email, password)
            saveToPreferences(email, password)
        }
    }

    override fun saveTeacherInfo(name: String, surname: String, post: String) {
        teacher = Teacher(uid = uid, name =  name,surname =  surname, post =  post)
        GlobalScope.launch {
            repositoryImpl.createNewTeacher(teacher)
            view?.navigate(AuthFragmentDirections.actionAuthFragmentToGroupsFragment(uid))
        }
    }

    override fun saveStudentInfo(name: String, surname: String, group: String, workLink: String) {
        student = Student(uid = uid, name = name, surname =  surname, group =  group, work_link =  workLink)
        GlobalScope.launch {
            repositoryImpl.createNewStudent(student)
            view?.navigate(AuthFragmentDirections.actionAuthFragmentToGroupsFragment(uid))
        }
    }

    private fun saveToPreferences(email: String, password: String) {
        sharedPreferencesUtil.setValue(email, EMAIL_KEY)
        sharedPreferencesUtil.setValue(password, PASSWORD_KEY)
    }

    override fun attachView(view: AuthContract.View) {
        super.attachView(view)
        GlobalScope.launch { tryToLoginWithSharedPreferences() }
    }

    private suspend fun tryToLoginWithSharedPreferences() {
        val email = sharedPreferencesUtil.getStringValue(EMAIL_KEY)
        val password = sharedPreferencesUtil.getStringValue(PASSWORD_KEY)
        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            view?.showLoginAction()
        } else {
            uid = repositoryImpl.logIn(email, password).user!!.uid
            delay(1000)
            view?.navigate(AuthFragmentDirections.actionAuthFragmentToGroupsFragment(uid))
        }
    }
}