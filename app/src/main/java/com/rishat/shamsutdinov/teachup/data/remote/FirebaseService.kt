package com.rishat.shamsutdinov.teachup.data.remote

import com.rishat.shamsutdinov.teachup.domain.entity.GroupResponce
import com.rishat.shamsutdinov.teachup.domain.entity.Student
import com.rishat.shamsutdinov.teachup.domain.entity.StudyGroup
import com.rishat.shamsutdinov.teachup.domain.entity.Teacher
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface FirebaseService {

    @POST("/users/{uid}.json")
    suspend fun createTeacher(@Path("uid") uid: String, @Body user: Teacher)

    @POST("/users/{uid}.json")
    suspend fun createStudent(@Path("uid") uid : String, @Body user : Student)

    @POST("/groups/{code}.json")
    suspend fun createNewGroup(@Path("code") code : String, @Body group : StudyGroup)

    @GET("/groups.json?print=pretty")
    fun getMyCreatedGroups() : Deferred<Response<GroupResponce>>
}