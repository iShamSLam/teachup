package com.rishat.shamsutdinov.teachup.presentation.auth

import com.rishat.shamsutdinov.teachup.presentation.base.BaseContract

interface AuthContract {
    interface View : BaseContract.View {
        fun showLoginAction()
        fun hideLoginAction()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun logInWithEmailAndPassword(email : String, password : String)
        fun signUpNewUser(email: String, password: String)
        fun saveTeacherInfo(name : String, surname : String, post : String)
        fun saveStudentInfo(name: String, surname: String, group : String, workLink : String)
    }
}