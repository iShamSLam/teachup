package com.rishat.shamsutdinov.teachup.presentation.groups

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.rishat.shamsutdinov.teachup.R
import com.rishat.shamsutdinov.teachup.domain.entity.StudyGroup
import com.rishat.shamsutdinov.teachup.presentation.base.BaseFragment
import com.rishat.shamsutdinov.teachup.presentation.groups.dialog.CreateGroupDialogHelper
import kotlinx.android.synthetic.main.groups_fragment.*
import javax.inject.Inject

class GroupsFragment : BaseFragment(), GroupsContract.View {

    @Inject
    lateinit var presenter: GroupsPresenter

    private val adapter = GroupsAdapter()

    override fun layoutId(): Int = R.layout.groups_fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent().inject(this)
        val args = GroupsFragmentArgs.fromBundle(requireArguments())
        presenter.onBindArgs(args.userId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        adapter.listener = object : GroupsAdapter.ClickListener {
            override fun onGroupTouched(code : String, name : String) {
                navigate(GroupsFragmentDirections.actionGroupsFragmentToStudentListFragment(code, name))
            }
        }
        rv_groups.adapter = adapter
        rv_groups.layoutManager = LinearLayoutManager(context)

        btn_create_group.setOnClickListener {
            showCreateGroupDialog()
        }
    }

    fun showCreateGroupDialog() {
        CreateGroupDialogHelper(context, object : CreateGroupDialogHelper.ClickListener {
            override fun onSave(name: String, code: String) {
                presenter.createGroup(name, code)
            }
        }).showDialog(true)
    }

    override fun updateData(group: StudyGroup) {
        val data = adapter.data
        data.add(group)
        adapter.data = data
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        clearData()
    }

    override fun clearData() {
        adapter.data.clear()
    }
}