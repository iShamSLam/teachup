package com.rishat.shamsutdinov.teachup.di.component

import com.rishat.shamsutdinov.teachup.di.module.ActivityModule
import com.rishat.shamsutdinov.teachup.di.module.AppModule
import com.rishat.shamsutdinov.teachup.di.module.NetModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetModule::class])
interface AppComponent {
    fun buildActivityComponent(activityModule: ActivityModule): ActivityComponent
}