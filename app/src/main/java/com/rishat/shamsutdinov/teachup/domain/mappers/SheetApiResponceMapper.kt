package com.rishat.shamsutdinov.teachup.domain.mappers

import com.rishat.shamsutdinov.teachup.domain.entity.StudentTask

object SheetApiResponceMapper {
    fun map(task: List<List<String>>): ArrayList<StudentTask> {
        return task.map { list ->
            StudentTask(list[0], list[1], list[2], list[3], list[4])
        }.drop(1)
            .toCollection(ArrayList<StudentTask>())
    }
}