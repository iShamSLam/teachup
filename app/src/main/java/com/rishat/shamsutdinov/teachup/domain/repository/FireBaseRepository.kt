package com.rishat.shamsutdinov.teachup.domain.repository

import com.google.firebase.auth.AuthResult
import com.rishat.shamsutdinov.teachup.domain.entity.Student
import com.rishat.shamsutdinov.teachup.domain.entity.StudentTask
import com.rishat.shamsutdinov.teachup.domain.entity.StudyGroup
import com.rishat.shamsutdinov.teachup.domain.entity.Teacher

interface FireBaseRepository {
    suspend fun logIn(email : String, password : String) : AuthResult
    suspend fun signUp(email: String, password: String) : String
    suspend fun createNewTeacher(teacher : Teacher)
    suspend fun createNewStudent(student: Student)
    suspend fun getStudentTasks(sheetId : String) : List<StudentTask>
    suspend fun createGroup(code : String, studyGroup: StudyGroup)
    suspend fun createChat()
}