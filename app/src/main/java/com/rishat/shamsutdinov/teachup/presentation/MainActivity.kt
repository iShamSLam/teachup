package com.rishat.shamsutdinov.teachup.presentation

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.rishat.shamsutdinov.teachup.R
import com.rishat.shamsutdinov.teachup.presentation.base.BaseActivity
import javax.inject.Inject

class MainActivity : BaseActivity(), MainContract.View {

    @Inject
    lateinit var presenter: MainActivityPresenter

    override fun layoutId(): Int = R.layout.activity_main

    private val navigationController: NavController
        get() = Navigation.findNavController(this, R.id.navHostFragment)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent().inject(this)
        presenter.attachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

//    override fun onBackPressed() {
//        when {
//            navigationController.currentDestination?.id == homeDestinationId -> finish()
//            else -> super.onBackPressed()
//        }
//    }
}