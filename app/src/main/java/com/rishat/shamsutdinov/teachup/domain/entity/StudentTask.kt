package com.rishat.shamsutdinov.teachup.domain.entity

data class StudentTask(
    val name: String,
    val description: String,
    val startDate: String,
    val endDate: String,
    val status: String
)