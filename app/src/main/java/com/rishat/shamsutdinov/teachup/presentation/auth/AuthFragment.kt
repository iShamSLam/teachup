package com.rishat.shamsutdinov.teachup.presentation.auth

import android.os.Bundle
import android.view.View
import com.rishat.shamsutdinov.teachup.R
import com.rishat.shamsutdinov.teachup.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_splash_auth.*
import javax.inject.Inject

class AuthFragment : BaseFragment(), AuthContract.View {

    override fun layoutId(): Int = R.layout.fragment_splash_auth

    @Inject
    lateinit var presenter: AuthPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        btn_login.setOnClickListener {
            presenter.logInWithEmailAndPassword(
                ed_log_login.text.toString(),
                ed_log_password.text.toString()
            )
        }
        btn_go_to_signup.setOnClickListener {
            showRegisterBlock()
        }

        btn_next.setOnClickListener {
            presenter.signUpNewUser(ed_log_login.text.toString(), ed_log_password.text.toString())
            showTypeSelectBlock()
        }

        btn_student.setOnClickListener {
            showStudentCreateBlock()
        }

        btn_teacher.setOnClickListener {
            showTeacherCreateBlock()
        }

        btn_st_create.setOnClickListener {
            presenter.saveStudentInfo(
                ed_st_name.text.toString(),
                ed_st_surname.text.toString(),
                ed_st_group.text.toString(),
                ed_link.text.toString()
            )
        }

        btn_te_create.setOnClickListener {
            presenter.saveTeacherInfo(
                ed_te_name.text.toString(),
                ed_st_surname.text.toString(),
                ed_te_post.text.toString()
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    override fun hideLoginAction() {
        cl_login_action.startAnimation(getAnim(R.anim.log_block_pop_out))
        cl_login_action.visibility = View.GONE
    }

    override fun showLoginAction() {
        cl_login_action.visibility = View.VISIBLE
        cl_login_action.startAnimation(getAnim(R.anim.log_block_popup))
    }

    private fun showRegisterBlock() {
        ln_buttons.visibility = View.GONE
        btn_next.visibility = View.VISIBLE
        tv_current_action.text = getText(R.string.sign_up_info)
    }

    private fun showTypeSelectBlock() {
        cl_login_action.visibility = View.GONE
        btn_next.visibility = View.GONE
        ln_select_type.visibility = View.VISIBLE
    }

    private fun showTeacherCreateBlock() {
        ln_select_type.visibility = View.GONE
        cl_create_teacher.visibility = View.VISIBLE
    }

    private fun showStudentCreateBlock() {
        ln_select_type.visibility = View.GONE
        cl_create_student.visibility = View.VISIBLE
    }
}