package com.rishat.shamsutdinov.teachup.domain.entity

import com.google.gson.annotations.SerializedName

data class StudyGroup(
    @SerializedName("code_phrase") val code_phrase : String = "",
    @SerializedName("creatorId") val creatorId : String = "",
    @SerializedName("name") val name : String = "",
    val student : List<Student> = emptyList()
)