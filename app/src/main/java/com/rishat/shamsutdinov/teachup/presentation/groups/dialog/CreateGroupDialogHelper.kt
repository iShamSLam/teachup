package com.rishat.shamsutdinov.teachup.presentation.groups.dialog

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import com.rishat.shamsutdinov.teachup.R
import kotlinx.android.synthetic.main.dialog_create_group.view.*

class CreateGroupDialogHelper(
    val context: Context?,
    val clickListener: ClickListener
) {
    private var dialog: AlertDialog? = null
    private var dialogView: View? = null

    fun showDialog(isShow: Boolean) {
        if (isShow) {
            if (context == null) return
            dialog?.dismiss()
            dialog = AlertDialog.Builder(context).create()
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_create_group, null)

            dialog?.setOnDismissListener {}

            dialogView?.btn_save?.setOnClickListener {
                clickListener.onSave(
                    dialogView?.ed_name?.text.toString(),
                    dialogView?.ed_code?.text.toString()
                )
                dialog?.dismiss()
            }

            dialog?.setView(dialogView)
            dialog?.setCancelable(true)
            dialog?.setCanceledOnTouchOutside(true)
            dialog?.show()
        } else {
            dialog?.dismiss()
        }
    }

    interface ClickListener {
        fun onSave(name: String, code: String)
    }
}