package com.rishat.shamsutdinov.teachup.di.module

import android.app.Activity
import androidx.fragment.app.Fragment
import dagger.Module
import dagger.Provides

@Module
class FragmentModule(private val fragment: Fragment) {

    @Provides
    fun provideFragment(): Fragment = fragment

    @Provides
    fun provideActivity(): Activity? = fragment.activity
}