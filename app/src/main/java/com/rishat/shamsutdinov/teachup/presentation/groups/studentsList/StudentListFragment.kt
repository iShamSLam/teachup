package com.rishat.shamsutdinov.teachup.presentation.groups.studentsList

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.rishat.shamsutdinov.teachup.R
import com.rishat.shamsutdinov.teachup.domain.entity.Student
import com.rishat.shamsutdinov.teachup.domain.entity.StudyGroup
import com.rishat.shamsutdinov.teachup.presentation.base.BaseFragment
import com.rishat.shamsutdinov.teachup.presentation.groups.GroupsFragmentArgs
import kotlinx.android.synthetic.main.fragment_student_list.*
import kotlinx.android.synthetic.main.groups_fragment.*
import javax.inject.Inject

class StudentListFragment : BaseFragment(), StudentListContract.View {

    @Inject
    lateinit var presenter: StudentListPresenter

    private val adapter = StudentListAdapter()

    private var titleText = ""

    override fun layoutId(): Int = R.layout.fragment_student_list

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent().inject(this)
        presenter.attachView(this)
        val args = StudentListFragmentArgs.fromBundle(requireArguments())
        presenter.onBindArgs(args.groupCode)
        titleText = args.groupName
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        title.text = titleText
        adapter.listener = object : StudentListAdapter.ClickListener{
            override fun onTablePressed(author : String ,link: String) {
                navigate(StudentListFragmentDirections.actionStudentListToWorkTableFragment(author, link))
            }
        }
        rv_students.adapter = adapter
        rv_students.layoutManager = LinearLayoutManager(context)

        btn_back.setOnClickListener{
            super.onBackPressed()
        }
    }

    override fun updateData(student: Student) {
        val data = adapter.data
        data.add(student)
        adapter.data = data
    }

    override fun clear() {
        adapter.data.clear()
    }
}