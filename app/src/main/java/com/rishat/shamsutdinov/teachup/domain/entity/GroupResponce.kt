package com.rishat.shamsutdinov.teachup.domain.entity

data class GroupResponce(
    val anotherResponce: AnotherResponce
)

data class AnotherResponce(
    val group: StudyGroup = StudyGroup()
)