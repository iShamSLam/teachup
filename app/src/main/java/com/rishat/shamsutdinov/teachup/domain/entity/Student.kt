package com.rishat.shamsutdinov.teachup.domain.entity

import com.google.gson.annotations.SerializedName
import java.security.acl.Group

data class Student(
    @SerializedName("uid") val uid : String = "",
    @SerializedName("name") val name : String = "",
    @SerializedName("surname") val surname : String = "",
    @SerializedName("group") val group : String = "",
    @SerializedName("work_link") val work_link : String = "",
    @SerializedName("subcribeGroup") val subcribedGroup : String = "",
    @SerializedName("isTeacher") val isTeacher : Boolean = false,
    @SerializedName("percent") val workPercent : String = "80"
)