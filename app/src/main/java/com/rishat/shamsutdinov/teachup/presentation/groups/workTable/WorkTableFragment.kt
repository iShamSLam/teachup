package com.rishat.shamsutdinov.teachup.presentation.groups.workTable

import android.os.Bundle
import android.view.View
import android.widget.Adapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.rishat.shamsutdinov.teachup.R
import com.rishat.shamsutdinov.teachup.domain.entity.StudentTask
import com.rishat.shamsutdinov.teachup.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_work_table.*
import javax.inject.Inject

class WorkTableFragment : BaseFragment(), WorkTableContract.View {

    @Inject
    lateinit var presenter: WorkTablePresenter

    private var titleText = ""

    private var adapter = WorkTableAdapter()

    override fun layoutId(): Int = R.layout.fragment_work_table

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent().inject(this)
        val args = WorkTableFragmentArgs.fromBundle(requireArguments())
        presenter.onBindArgs(link = args.link)
        titleText = args.author
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        rv_tasks.adapter = adapter
        title.text = titleText
        rv_tasks.layoutManager = LinearLayoutManager(context)

        btn_back.setOnClickListener{
            super.onBackPressed()
        }
    }

    override fun updateData(data: ArrayList<StudentTask>) {
        adapter.data = data
    }

    override fun clear() {
        adapter.data.clear()
    }
}