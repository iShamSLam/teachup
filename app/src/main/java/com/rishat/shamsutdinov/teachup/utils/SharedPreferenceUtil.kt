package com.rishat.shamsutdinov.teachup.utils

import android.content.Context

class SharedPreferencesUtil(private val context: Context) {

    companion object {
        private const val PREFS_NAME = "SharedPreferencesFile"
    }

    fun setValue(value: String, key: String) {
        val prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        prefs.edit().putString(key, value).apply()
    }

    fun getStringValue(key: String): String? {
        val prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        return prefs.getString(key, "")
    }

    fun clean() {
        val editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.apply()
    }
}