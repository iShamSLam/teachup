package com.rishat.shamsutdinov.teachup.presentation.base

import android.app.Activity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.collection.LongSparseArray
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import com.rishat.shamsutdinov.teachup.App
import com.rishat.shamsutdinov.teachup.R
import com.rishat.shamsutdinov.teachup.di.component.ActivityComponent
import com.rishat.shamsutdinov.teachup.di.module.ActivityModule
import com.rishat.shamsutdinov.teachup.utils.PermissionUtil
import java.util.concurrent.atomic.AtomicLong

abstract class BaseActivity : AppCompatActivity(), BaseContract.View {

    companion object {
        private const val KEY_ACTIVITY_ID = "KEY_ACTIVITY_ID"
        private val ACTIVITY_ID = AtomicLong(0)
        private val components = LongSparseArray<ActivityComponent>()
    }

    var activityComponent: ActivityComponent? = null
        private set
    private var activityId = 0L

    @LayoutRes
    abstract fun layoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        activityId = savedInstanceState?.getLong(KEY_ACTIVITY_ID) ?: ACTIVITY_ID.getAndIncrement()
        if (components.get(activityId) == null) {
            activityComponent = App.appComponent.buildActivityComponent(ActivityModule(this))
            components.put(activityId, activityComponent)
        } else {
            activityComponent = components.get(activityId)
        }
        activityComponent?.inject(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(KEY_ACTIVITY_ID, activityId)
    }

    override fun onDestroy() {
        if (!isChangingConfigurations) {
            components.remove(activityId)
        }
        super.onDestroy()
    }

    fun activityComponent() = activityComponent as ActivityComponent

    override fun activity(): Activity = this

    override fun showMessage(stringResId: Int) =
        Toast.makeText(this, stringResId, Toast.LENGTH_LONG).show()

    override fun navigate(@IdRes resId: Int) =
        Navigation.findNavController(activity(), R.id.navHostFragment).navigate(resId)

    override fun navigate(navDirections: NavDirections) =
        Navigation.findNavController(activity(), R.id.navHostFragment).navigate(navDirections)

    override fun popBackStack(): Boolean =
        Navigation.findNavController(activity(), R.id.navHostFragment).popBackStack()

    fun isCanNavigateUp(): Boolean {
        val host = supportFragmentManager.findFragmentById(R.id.navHostFragment)
        return if (host != null) {
            host.childFragmentManager.backStackEntryCount > 1
        } else {
            false
        }
    }

    override fun onBackPressed() {
        val host = supportFragmentManager.findFragmentById(R.id.navHostFragment)
        val topFragment = host?.childFragmentManager?.findFragmentById(R.id.navHostFragment)
        if (topFragment is BaseFragment) {
            if (!topFragment.onBackPressed()) {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }

    override fun getStringRes(stringResId: Int): String = getString(stringResId)

    override fun hideKeyboard() {
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
    }

    protected open fun requiredPermissions() = arrayOf<String>()

    protected open fun requestCodePermissions() = 0

    override fun isPermissionGranted() =
        PermissionUtil.isPermissionsGranted(activity(), *requiredPermissions())

    override fun requestRequiredPermissions() {
        requestPermissions(requiredPermissions(), requestCodePermissions())
    }

    override fun getPermissionsState() =
        PermissionUtil.getPermissionsState(activity(), *requiredPermissions())

    override fun showRequestPermissionsSnackbar(stringResId: Int) {/*empty*/
    }
}