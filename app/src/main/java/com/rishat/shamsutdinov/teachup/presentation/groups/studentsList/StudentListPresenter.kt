package com.rishat.shamsutdinov.teachup.presentation.groups.studentsList

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.rishat.shamsutdinov.teachup.data.repository.FireBaseRepositoryImpl
import com.rishat.shamsutdinov.teachup.domain.entity.Student
import com.rishat.shamsutdinov.teachup.presentation.base.BasePresenter
import javax.inject.Inject

class StudentListPresenter @Inject constructor(private val database : FirebaseDatabase) : BasePresenter<StudentListContract.View>(), StudentListContract.Presenter {

    private var groupCode = ""

    override fun onBindArgs(code: String) {
        groupCode = code
    }

    override fun attachView(view: StudentListContract.View) {
        super.attachView(view)
        getData()
    }

    private fun getData() {
        val ref = database.getReference("groups")
        ref.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                snapshot.children.forEach {
                    if (it.child("code_phrase").value == groupCode) {
                        it.child("student").children.forEach { student ->
                            val data = student.getValue(Student::class.java)
                            if (data != null) {
                                view?.updateData(data)
                            }
                        }
                    }
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                TODO("Not yet implemented")
            }

        })
    }
}