package com.rishat.shamsutdinov.teachup.presentation.groups.workTable

import com.rishat.shamsutdinov.teachup.domain.entity.StudentTask
import com.rishat.shamsutdinov.teachup.presentation.base.BaseContract

interface WorkTableContract {
    interface View : BaseContract.View{
        fun updateData(data : ArrayList<StudentTask>)
        fun clear()
    }
    interface Presenter : BaseContract.Presenter<View>{
        fun onBindArgs(link : String)
        fun getData()
    }
}