package com.rishat.shamsutdinov.teachup.di.module

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.rishat.shamsutdinov.teachup.data.remote.FirebaseService
import com.rishat.shamsutdinov.teachup.data.remote.SheetsService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient().newBuilder().build()

    @Provides
    @Singleton
    fun provideGsonConverterFactory(): GsonConverterFactory = GsonConverterFactory.create()


    @Provides
    @Singleton
    fun provideFireBaseApi(retrofit: Retrofit): FirebaseService =
        retrofit.create(FirebaseService::class.java)

    @Provides
    @Singleton
    fun provideSheetApi(retrofit: Retrofit) : SheetsService =
        retrofit.create(SheetsService::class.java)

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient, converterFactory: GsonConverterFactory): Retrofit =
        Retrofit.Builder()
            .baseUrl("https://geoq-668bb.firebaseio.com")
            .client(client)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()

    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth = FirebaseAuth.getInstance()

    @Provides
    @Singleton
    fun provideDatabase() : FirebaseDatabase = FirebaseDatabase.getInstance()
}