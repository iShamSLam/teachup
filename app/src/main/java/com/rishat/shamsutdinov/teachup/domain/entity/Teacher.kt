package com.rishat.shamsutdinov.teachup.domain.entity

data class Teacher(
    val uid : String = "",
    val name : String = "",
    val surname : String = "",
    val post : String = "",
    val isTeacher : Boolean = true
)