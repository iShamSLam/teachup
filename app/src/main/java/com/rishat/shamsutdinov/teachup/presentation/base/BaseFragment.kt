package com.rishat.shamsutdinov.teachup.presentation.base

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.collection.LongSparseArray
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.rishat.shamsutdinov.teachup.R
import com.rishat.shamsutdinov.teachup.di.component.FragmentComponent
import com.rishat.shamsutdinov.teachup.di.module.FragmentModule
import com.rishat.shamsutdinov.teachup.utils.PermissionUtil
import java.util.concurrent.atomic.AtomicLong

abstract class BaseFragment : Fragment(), BaseContract.View {

    companion object {
        private const val KEY_FRAGMENT_ID = "KEY_FRAGMENT_ID"
        private val FRAGMENT_ID = AtomicLong(0)
        private val components = LongSparseArray<FragmentComponent>()
        private const val PERMISSIONS_SNACKBAR_DURATION = 6 * 1000 //milliseconds
    }

    var fragmentComponent: FragmentComponent? = null
        private set
    private var fragmentId = 0L

    @LayoutRes
    abstract fun layoutId(): Int

    private var snackbarPermissions: Snackbar? = null
    protected var isDialogShowing: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentId = savedInstanceState?.getLong(KEY_FRAGMENT_ID) ?: FRAGMENT_ID.getAndIncrement()
        if (components.get(fragmentId) == null) {
            fragmentComponent = (activity as BaseActivity).activityComponent()
                .buildFragmentComponent(FragmentModule(this))
            components.put(fragmentId, fragmentComponent)
        } else {
            fragmentComponent = components.get(fragmentId)
        }
        fragmentComponent?.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId(), container, false)
    }

    protected open fun isToolbarNeeded() = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    protected fun isChangingConfiguration(): Boolean = activity?.isChangingConfigurations ?: false

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(KEY_FRAGMENT_ID, fragmentId)
    }

    override fun onDestroy() {
        hideKeyboard()
        if (!isChangingConfiguration()) {
            snackbarPermissions?.dismiss()
            components.remove(fragmentId)
        }
        super.onDestroy()
    }

    fun fragmentComponent() = fragmentComponent as FragmentComponent

    override fun activity() = activity as Activity

    override fun showMessage(stringResId: Int) =
        Toast.makeText(context, stringResId, Toast.LENGTH_LONG).show()

    override fun navigate(@IdRes resId: Int) =
        Navigation.findNavController(activity(), R.id.navHostFragment).navigate(resId)

    override fun navigate(navDirections: NavDirections) =
        Navigation.findNavController(activity(), R.id.navHostFragment).navigate(navDirections)

    override fun popBackStack(): Boolean =
        Navigation.findNavController(activity(), R.id.navHostFragment).popBackStack()

    open fun onBackPressed(): Boolean {
        return false
    }

    protected open fun isDeviceDisconnectedHandlingNeeded() = false


    override fun getStringRes(stringResId: Int) = getString(stringResId)

    override fun hideKeyboard() {
        (activity as BaseActivity).hideKeyboard()
    }

    override fun isPermissionGranted() =
        PermissionUtil.isPermissionsGranted(activity(), *requiredPermissions())

    protected open fun requiredPermissions() = arrayOf<String>()

    protected open fun requestCodePermissions() = 0

    protected open fun getAnim(intRes : Int) : Animation {
       return AnimationUtils.loadAnimation(context, intRes)
    }

    override fun requestRequiredPermissions() {
        requestPermissions(requiredPermissions(), requestCodePermissions())
    }

    override fun getPermissionsState() =
        PermissionUtil.getPermissionsState(activity(), *requiredPermissions())

    @SuppressLint("WrongConstant", "UseRequireInsteadOfGet")
    override fun showRequestPermissionsSnackbar(stringResId: Int) {
        if (context == null || this.view == null) return
        snackbarPermissions =
            Snackbar
                .make(this.view!!, stringResId, PERMISSIONS_SNACKBAR_DURATION)
                .setAction(R.string.settings) {
                    startActivity(
                        Intent(
                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", context!!.packageName, null)
                        )
                    )
                }
                .apply {
                    view.setBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.colorPrimaryDark
                        )
                    )
                    (view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView)
                    show()
                }
    }
}