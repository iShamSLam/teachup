package com.rishat.shamsutdinov.teachup.presentation

import com.rishat.shamsutdinov.teachup.di.scope.ActivityScope
import com.rishat.shamsutdinov.teachup.presentation.MainContract
import com.rishat.shamsutdinov.teachup.presentation.base.BasePresenter
import javax.inject.Inject

@ActivityScope
class MainActivityPresenter @Inject constructor() : BasePresenter<MainContract.View>(), MainContract.Presenter