package com.rishat.shamsutdinov.teachup.presentation

import com.rishat.shamsutdinov.teachup.presentation.base.BaseContract

interface MainContract {

    interface View : BaseContract.View

    interface Presenter : BaseContract.Presenter<View>
}