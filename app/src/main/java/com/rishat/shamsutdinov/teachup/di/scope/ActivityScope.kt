package com.rishat.shamsutdinov.teachup.di.scope

import javax.inject.Scope

@Scope
@Retention
annotation class ActivityScope