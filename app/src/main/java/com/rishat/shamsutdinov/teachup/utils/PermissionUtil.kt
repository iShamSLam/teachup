package com.rishat.shamsutdinov.teachup.utils

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat

object PermissionUtil {

    enum class PermissionState(val priority: Int) {
        GRANTED(1),
        DENIED(2),
        NEVER_ASK_AGAIN(3)
    }

    private fun getPermissionState(activity: Activity, permission: String): PermissionState {
        return if (ActivityCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED) {
            PermissionState.GRANTED
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                PermissionState.DENIED
            } else {
                PermissionState.NEVER_ASK_AGAIN
            }
        }
    }

    fun getPermissionsState(activity: Activity, vararg permissions: String): PermissionState {
        if (permissions.isNullOrEmpty()) return PermissionState.GRANTED
        return permissions
            .map { getPermissionState(activity, it) }
            .maxBy{ it.priority }
            ?: throw IllegalArgumentException("Invalid permissions")
    }

    fun isPermissionsGranted(activity: Activity, vararg permissions: String) =
        getPermissionsState(activity, *permissions) == PermissionState.GRANTED
}