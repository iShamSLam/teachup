package com.rishat.shamsutdinov.teachup.presentation.groups

import com.rishat.shamsutdinov.teachup.domain.entity.StudyGroup
import com.rishat.shamsutdinov.teachup.presentation.base.BaseContract

interface GroupsContract {
    interface View : BaseContract.View {
        fun updateData(group : StudyGroup)
        fun clearData()
    }
    interface Presenter : BaseContract.Presenter<View> {
        fun onBindArgs(uid : String)
        fun getGroups(creatorId :  String)
        fun createGroup(name : String, code : String)
    }
}