package com.rishat.shamsutdinov.teachup.di.component

import com.rishat.shamsutdinov.teachup.presentation.MainActivity
import com.rishat.shamsutdinov.teachup.di.module.ActivityModule
import com.rishat.shamsutdinov.teachup.di.module.FragmentModule
import com.rishat.shamsutdinov.teachup.di.scope.ActivityScope
import com.rishat.shamsutdinov.teachup.presentation.base.BaseActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [ActivityModule::class])
interface ActivityComponent {
    fun inject(baseActivity: BaseActivity)
    fun inject(mainActivity: MainActivity)
    fun buildFragmentComponent(fragmentModule: FragmentModule): FragmentComponent
}