package com.rishat.shamsutdinov.teachup.di.component

import com.rishat.shamsutdinov.teachup.di.module.FragmentModule
import com.rishat.shamsutdinov.teachup.di.scope.FragmentScope
import com.rishat.shamsutdinov.teachup.presentation.auth.AuthFragment
import com.rishat.shamsutdinov.teachup.presentation.base.BaseFragment
import com.rishat.shamsutdinov.teachup.presentation.groups.GroupsFragment
import com.rishat.shamsutdinov.teachup.presentation.groups.studentsList.StudentListFragment
import com.rishat.shamsutdinov.teachup.presentation.groups.workTable.WorkTableFragment
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [FragmentModule::class])
interface FragmentComponent {
    fun inject(baseFragment: BaseFragment)
    fun inject(authFragment: AuthFragment)
    fun inject(groupsFragment: GroupsFragment)
    fun inject(studentListFragment: StudentListFragment)
    fun inject(workTableFragment: WorkTableFragment)
}