package com.rishat.shamsutdinov.teachup.presentation.groups.studentsList

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rishat.shamsutdinov.teachup.R
import com.rishat.shamsutdinov.teachup.domain.entity.Student
import com.rishat.shamsutdinov.teachup.domain.entity.StudyGroup
import com.rishat.shamsutdinov.teachup.utils.inflate
import kotlinx.android.synthetic.main.item_group.view.*
import kotlinx.android.synthetic.main.item_student.view.*

class StudentListAdapter : RecyclerView.Adapter<StudentListAdapter.StudentViewHolder>() {
    interface ClickListener {
        fun onTablePressed(author : String, link: String)
    }

    var listener: ClickListener? = null

    var data: ArrayList<Student> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class StudentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(student: Student) = itemView.apply {
            tv_name.text = student.name
            surname.text = student.surname
            tv_percent.text = student.workPercent
            rv_group.text = student.group

            iv_table.setOnClickListener {
                listener?.onTablePressed("${student.name} ${student.surname}", student.work_link)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        return StudentViewHolder(parent.inflate(R.layout.item_student))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        holder.bind(data[position])
    }
}