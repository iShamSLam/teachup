package com.rishat.shamsutdinov.teachup.presentation.groups.workTable

import com.rishat.shamsutdinov.teachup.data.repository.FireBaseRepositoryImpl
import com.rishat.shamsutdinov.teachup.presentation.base.BasePresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.Dispatcher
import javax.inject.Inject

class WorkTablePresenter @Inject constructor(private val repositoryImpl: FireBaseRepositoryImpl) :
    BasePresenter<WorkTableContract.View>(), WorkTableContract.Presenter {

    private var link: String = ""

    override fun onBindArgs(link: String) {
        this.link = link
    }

    override fun getData() {
        GlobalScope.launch {
            val data = repositoryImpl.getStudentTasks(link)
            launch(Dispatchers.Main) {
                view?.updateData(data = data)
            }
        }
    }

    override fun attachView(view: WorkTableContract.View) {
        super.attachView(view)
        getData()
    }
}