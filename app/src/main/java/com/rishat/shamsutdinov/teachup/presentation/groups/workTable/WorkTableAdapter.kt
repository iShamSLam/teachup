package com.rishat.shamsutdinov.teachup.presentation.groups.workTable

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rishat.shamsutdinov.teachup.R
import com.rishat.shamsutdinov.teachup.domain.entity.StudentTask
import com.rishat.shamsutdinov.teachup.utils.inflate
import kotlinx.android.synthetic.main.item_task.view.*

class WorkTableAdapter : RecyclerView.Adapter<WorkTableAdapter.WorkTableViewHolder>() {
    interface ClickListener {
        fun onTablePressed(author: String, link: String)
    }

    var listener: ClickListener? = null

    var data: ArrayList<StudentTask> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class WorkTableViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(student: StudentTask) = itemView.apply {
            tv_start.text = "${tv_start.text} ${student.startDate}"
            tv_end.text = "${tv_end.text} ${student.endDate}"
            tv_task_name.text = "${tv_task_name.text} ${student.name}"
            tv_status.text = "${tv_status.text} ${student.status}"
            tv_description_body.text = student.description

            btn_open_close.setOnClickListener {
                if (cl_description.visibility == View.GONE) {
                    btn_open_close.rotationX = 180f
                    cl_description.visibility = View.VISIBLE
                } else {
                    btn_open_close.rotationX = 180f
                    cl_description.visibility = View.GONE
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkTableViewHolder {
        return WorkTableViewHolder(parent.inflate(R.layout.item_task))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: WorkTableViewHolder, position: Int) {
        holder.bind(data[position])
    }
}