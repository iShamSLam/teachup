package com.rishat.shamsutdinov.teachup.presentation.groups

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.rishat.shamsutdinov.teachup.data.repository.FireBaseRepositoryImpl
import com.rishat.shamsutdinov.teachup.di.scope.FragmentScope
import com.rishat.shamsutdinov.teachup.domain.entity.Student
import com.rishat.shamsutdinov.teachup.domain.entity.StudyGroup
import com.rishat.shamsutdinov.teachup.presentation.auth.AuthContract
import com.rishat.shamsutdinov.teachup.presentation.base.BasePresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@FragmentScope
class GroupsPresenter @Inject constructor(private val repositoryImpl: FireBaseRepositoryImpl, private val database : FirebaseDatabase) :
    BasePresenter<GroupsContract.View>(), GroupsContract.Presenter {

    private var userId = ""

    override fun onBindArgs(uid: String) {
        userId = uid
    }

    override fun getGroups(creatorId: String) {
        database.getReference("groups").addChildEventListener(object : ChildEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                snapshot.children.forEach{
                    val group = it.getValue(StudyGroup::class.java)
                    if (group != null) {
                        view?.updateData(group)
                    }
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                TODO("Not yet implemented")
            }

        })
    }

    override fun createGroup(name: String, code: String) {
        GlobalScope.launch {
            repositoryImpl.createGroup(
                code,
                StudyGroup(code_phrase = code, name = name, creatorId = userId)
            )
        }
    }

    override fun attachView(view: GroupsContract.View) {
        super.attachView(view)
        getGroups(userId)
    }
}