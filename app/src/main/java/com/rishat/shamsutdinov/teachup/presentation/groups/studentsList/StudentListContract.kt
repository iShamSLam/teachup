package com.rishat.shamsutdinov.teachup.presentation.groups.studentsList

import com.rishat.shamsutdinov.teachup.domain.entity.Student
import com.rishat.shamsutdinov.teachup.domain.entity.StudyGroup
import com.rishat.shamsutdinov.teachup.presentation.base.BaseContract

interface StudentListContract {
    interface View : BaseContract.View {
        fun updateData(student : Student)
        fun clear()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun onBindArgs(code : String)
    }
}